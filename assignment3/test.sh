#!/bin/bash

echo "Checking files in current directory"
ls

echo "Printing calendar"
cal

echo "Moving back a directory"
cd ..

echo "Printing user environment variable "
printenv | grep "USER"

echo "Printing home environment variable"
printenv | grep "HOME"
