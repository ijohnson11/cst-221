#include <stdio.h>
#include <stdlib.h>

#define N 100


void producer()
{
    int i;
    
    while(1)
    {
        i = produce();
        put(i);
    }
}


void consumer()
{
    int i;
    
    while(1)
    {
        i = get();
        consume(i);
    }
}

int main()
{
    pthread_t id;
    pthread_create(&id, NULL, busy, "Hi");
    pthread_create(&id, NULL, busy1, "Ola");
}