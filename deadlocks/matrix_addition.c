#include <stdio.h>

int main()
{
    int m, n, c, d, first[10][10], second[10][10], sum[10][10];

    printf("Enter the number of rows and cols in the matrix: ");
    scanf("%d%d", &m, &n);
    printf("Enter the elements of the first matrix: ");

    for ( c = 0; c < m ; c++)
    {
        for (d = 0; d < n; d++)
        {
            scanf("%d", &first[c][d]);
            printf("%d\n", first[c][d]);
        }
    }
    
    printf("Enter the elements of the second matrix: ");

    for ( c = 0; c < m ; c++)
    {
        for (d = 0; d < n; d++)
        {
            scanf("%d", &second[c][d]);
        }
    }


    printf("Sum of entered matrices: -\n");


    for (c = 0; c < m ; c++)
    {
        for (d = 0; d < n; d++)
        {
            sum[c][d] = first[c][d] + second[c][d];
            printf("%d\t", sum[c][d]);
            
        }
        printf("\n");
    }
    
    return 0;
}