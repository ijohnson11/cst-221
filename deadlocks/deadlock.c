#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

typedef int bool;
#define true 1
#define false 0

int resource1_owner, resource1_mutex;
bool process1_starved, process2_starved;
FILE *fp;

void *process1(void *ptr)
{
    while(1){
        grabResource(1);
        if(resource1_owner == 1)
        {
            process1_starved = false;
            useResource(1);
            releaseResource(1);
            sleep(2);    
        }

        else{
            fp = fopen("./activitylog.txt", "a+");
            printf("Process1 starved... Starting reset timer\n");
            fprintf(fp, "Process1 starved... Starting reset timer\n");
            fclose(fp);
            process1_starved = true;
            setResetTimer(5);
        }
         
    }
    return NULL;
}


void *process2(void *ptr)
{
    while(1){
        grabResource(2);
        if(resource1_owner == 2)
        {
            process2_starved = false;
            useResource(2);
            releaseResource(2);
            sleep(2);
        }
        else{
            fp = fopen("./activitylog.txt", "a+");
            printf("Process2 is starved... Staring reset timer\n");
            fprintf(fp, "Process2 is starved... Staring reset timer\n");
            fclose(fp);
            process2_starved = true;
            setResetTimer(5);
        }
    }
    return NULL;
}



int main()
{
    resource1_mutex = 1;
    resource1_owner = 0;
    pthread_t id;
    pthread_create(&id, NULL, process1, NULL);
    pthread_create(&id, NULL, process2, NULL);
    while(1){}
}

void grabResource(int process_number)
{
    if(resource1_mutex == 1)
    {
        fp = fopen("./activitylog.txt", "a+");
        printf("Process%d grabbing resource1\n", process_number);
        fprintf(fp, "Process%d grabbing resource1\n", process_number);
        resource1_mutex--;
        resource1_owner = process_number;
        fclose(fp);
    }
    else
    {
        fp = fopen("./activitylog.txt", "a+");
        printf("Process %d cannot grab resource1\n", process_number);
        fprintf(fp, "Process %d cannot grab resource1\n", process_number);
        fclose(fp);
    }
}

void useResource(int process_number)
{
    fp = fopen("./activitylog.txt", "a+");
    printf("Process%d using resource1..\n", process_number);
    fprintf(fp, "Process%d using resource1..\n", process_number);
    fclose(fp);
    sleep(2);
}

void releaseResource(int process_number)
{
    fp = fopen("./activitylog.txt", "a+");
    printf("Process%d releasing resource1...\n", process_number);
    fprintf(fp, "Process%d releasing resource1...\n", process_number);
    fclose(fp);
    resource1_owner = 0;
    resource1_mutex++;
}

void setResetTimer(int sleeptime)
{
    sleep(sleeptime);
}
