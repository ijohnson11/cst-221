#include <stdio.h>
#include <string.h>

int main()
{
    char input[1000];
    printf("Input String: ");
    scanf("%[^\n]", input);
    printf("%s\n\n", input);
    
    int array_length = strlen(input);
    
    printf("Array Length: %d\n", array_length);
    for (int i = 0; i < array_length; i++)
    {
        int charCode = input[i];
        
        if(charCode == 9)
        {
            input[i] = 32;
            for(int j = array_length ; j > i; j--)
            {
                input[j+3] = input[j];
                input[j] = 32;
            }
        }
    }
    
    for (int i = 0; i < array_length; i++)
    {
        int charCode = input[i];
        printf("%d\n", charCode);
    }
    
    
    printf("New String: %s\n", input);
}